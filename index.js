(function main() {

    let heartBeatingStop;
    let shakingStop;

    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            getAnimaster().fadeIn(block, 5000);
        });

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            getAnimaster().move(block, 1000, { x: 100, y: 10 });
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            getAnimaster().scale(block, 1000, 1.25);
        });

    document.getElementById('fadeOutPlay')
    .addEventListener('click', function () {
        const block = document.getElementById('fadeOutBlock');
        getAnimaster().fadeOut(block, 5000);
    });

    document.getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
        const block = document.getElementById('moveAndHideBlock');
        getAnimaster().moveAndHide(block, 5000);
    });

    document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
        const block = document.getElementById('showAndHideBlock');
        getAnimaster().showAndHide(block, 5000);
    });

    document.getElementById('heartBeatingPlay')
    .addEventListener('click', function () {
        const block = document.getElementById('heartBeatingBlock');
        heartBeatingStop = getAnimaster().heartBeating(block, 500);
    });

    document.getElementById('shakingPlay')
    .addEventListener('click', function () {
        const block = document.getElementById('shakingBlock');
        shakingStop = getAnimaster().shaking(block, 500);
    });

    document.getElementById('heartBeatingStop')
    .addEventListener('click', function () {
        heartBeatingStop.stop();
    });

    document.getElementById('shakingStop')
    .addEventListener('click', function () {
        shakingStop.stop();
    });

    document.getElementById('moveAndHideReset')
    .addEventListener('click', function () {
        const block = document.getElementById('moveAndHideBlock');
        getAnimaster().resetMoveAndScale(block);
        getAnimaster().resetFadeOut(block);
    })

})();

/**
 * Блок плавно появляется из прозрачного.
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 */

function getAnimaster(params) {
    let animaster = {};

    animaster.move = function (element, duration, translation) {
    element.style.transitionDuration = `${duration}ms`;
    element.style.transform = getTransform(translation, null);
    }

    animaster.scale = function (element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(null, ratio);
    }
    animaster.fadeIn = function (element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
    }
    animaster.fadeOut = function (element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
    }

    animaster.moveAndHide = function (element, duration) {
        animaster.move(element, duration * 2 / 5, { x: 100, y: 20 });
        animaster.fadeOut(element, duration * 3 / 5);
    }

    animaster.showAndHide = function (element, duration) {
        animaster.fadeIn(element, duration / 3);
        setTimeout(() => { this.fadeOut(element, duration / 3); }, duration / 3);
    }

    animaster.heartBeating = function (element, duration) {
        let timerId = setInterval(() => {
                this.scale(element, duration, 1.4)
                setTimeout(() => {this.scale(element, duration, 0.6)}, duration);
            }, duration*2);
            let heartBeat = {};
            heartBeat.stop = function () {
                clearInterval(timerId);
            } 
            heartBeat.timerId = timerId;
        return  heartBeat;                   
    }

    animaster.shaking = function (element, duration) {
        let timerId = setInterval(() => {
            this.move(element, duration, { x: -20, y: 0 })
            setTimeout(() => {this.move(element, duration, { x: 20, y: 0 })}, duration);
        }, duration*2);
        let shaking = {};
        shaking.stop = function () {
            clearInterval(timerId);
        }
        return shaking;
    }

    animaster.resetFadeIn = function (element) {
        element.classList.remove('show');
        element.classList.add('hide');
        element.style.transitionDuration = null;
    }

    animaster.resetFadeOut = function (element) {
        element.classList.remove('hide');
        element.classList.add('show');
        element.style.transitionDuration = null;
    }

    animaster.resetMoveAndScale = function (element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    }

    return animaster;
}



/**
 * Функция, передвигающая элемент
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 * @param translation — объект с полями x и y, обозначающими смещение блока
 */

/**
 * Функция, увеличивающая/уменьшающая элемент
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
 */


function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(' ');
}